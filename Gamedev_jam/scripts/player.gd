extends "character.gd"

var cam = null;
var tomato = "res://scenes/tomato.tscn";

func _ready():
	tomato = load(tomato);
	cam = get_node("cam");
	cam.excl.push_back(self);
	cam.set_active(true);
	set_process_input(true)

func _input(e):
	if e.type == InputEvent.MOUSE_BUTTON:
		attack(global_pos.basis[2])

func movement(delta):
	var dir = Vector3()
	var aim = cam.get_global_transform().basis;
	if Input.is_key_pressed(KEY_A): dir -= aim[0];
	if Input.is_key_pressed(KEY_D): dir += aim[0];
	if Input.is_key_pressed(KEY_W): dir -= aim[2];
	if Input.is_key_pressed(KEY_S): dir += aim[2];
	if Input.is_key_pressed(KEY_SPACE): jump();

	if dir.length()>0: lookat(dir,delta)
	return dir

func attack(dir):
	var inst = tomato.instance();
	inst.owner = self;
	inst.set_translation(get_translation()+dir+Vector3(0,1.5,0));
	inst.apply_impulse(Vector3(), dir*20);
	inst.set_angular_velocity(Vector3(1,1,1)*deg2rad(360));
	get_parent().add_child(inst, true);