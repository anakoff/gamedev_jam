extends "character.gd"

export(NodePath)var player
var tomato = "res://scenes/tomato.tscn";

func _ready():
	tomato = load(tomato);
	if !player: return 
	player = get_node(player)

func lookat(pos,delta,speed=5):
	if !player: return 
	var p = player.global_pos.origin-global_pos.origin
	global_pos.basis = Matrix3(Quat(global_pos.basis).slerp(Quat(Vector3(0,1,0), -atan2(p.x, p.z)), speed*delta));
	set_global_transform(global_pos)

func movement(delta):
	if !player: return Vector3()
	var dir = Vector3()
	var aim = global_pos.basis;
	dir += aim[2];
	if dir.length()>0: lookat(dir,delta)
	return dir

func attack():
	if !player: return
	var dist = get_dist(player)
	var dir = Vector3()
	if dist.length() < 20.0:
		dir.x = dist.x;
		dir.z = dist.z;
		dir = dir.normalized();
		
	var inst = tomato.instance();
	inst.owner = self;
	inst.set_translation(get_translation()+dir+Vector3(0,1.5,0));
	inst.apply_impulse(Vector3(), dir*20);
	inst.set_angular_velocity(Vector3(1,1,1)*deg2rad(360));
	get_parent().add_child(inst, true);

func kill():
	pass