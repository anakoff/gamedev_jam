extends "kb_obj.gd"

# Member variables
var g = -19.6;
var vel = Vector3();

# Constants
const MOVE_SPEED = 5.0;
const MAX_SPEED = 8.0;
const ACCEL = 10;
const DEACCEL = 16;
const MAX_SLOPE_ANGLE = 30;
const JUMP_FORCE = 10;
const JUMP_MAX = 12;
const ATTACK_DELAY = 0.5;

var on_floor = false;
var body_yaw = 0.0;
var global_pos = Transform()


func _ready():
	# Called every time the node is added to the scene.
	# Initialization here
	pass

func jump():
	if !on_floor: return 
	vel.y = JUMP_FORCE+(JUMP_MAX-JUMP_FORCE)*1;


func _fixed_process(delta):
	kb_update(delta)
	

func kb_update(delta):
	global_pos = get_global_transform();
	var dir = movement(delta)
	dir.y = 0;
	dir = dir.normalized();
	vel.y += g*delta
	var hvel = vel
	hvel.y = 0
	var move_speed = MOVE_SPEED+((MAX_SPEED-MOVE_SPEED)*1);
	var target = dir*move_speed;
	var accel
	if (dir.dot(hvel) > 0): accel = ACCEL
	else: accel = DEACCEL
	hvel = hvel.linear_interpolate(target, accel*delta)
	vel.x = hvel.x
	vel.z = hvel.z
	var motion = move(vel*delta)
	on_floor = false
	var original_vel = vel
	var floor_velocity = Vector3()
	var attempts = 4
	while(is_colliding() and attempts):
		var n = get_collision_normal();
		var p = get_collision_pos();
		var collider = get_collider();
		if collider extends RigidBody:
			collider.apply_impulse(p-collider.get_global_transform().origin, -n*collider.get_mass());
		#if vel.y <= -FALLATTACK_FORCE: if collider extends preload("res://scripts/zombie.gd"): collider.kill();
		if (rad2deg(acos(n.dot(Vector3(0, 1, 0)))) < MAX_SLOPE_ANGLE):
			floor_velocity = get_collider_velocity()
			on_floor = true
		motion = n.slide(motion)
		vel = n.slide(vel)
		if (original_vel.dot(vel) > 0):
			motion=move(motion)
			if (motion.length() < 0.001): break
		attempts -= 1
	if (on_floor and floor_velocity != Vector3()): move(floor_velocity*delta)
	

func lookat(pos,delta,speed=10):
	global_pos.basis = Matrix3(Quat(global_pos.basis).slerp(Quat(Vector3(0,1,0), -atan2(pos.x, pos.z)), speed*delta));
	set_global_transform(global_pos)

func movement(dir): return Vector3()

func get_dist(target, obj=self):
	return target.global_pos.origin-obj.global_pos.origin;