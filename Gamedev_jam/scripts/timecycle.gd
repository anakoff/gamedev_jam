extends Spatial

var env;
var dirlight;

func _ready():
	env = get_node("WorldEnvironment").get_environment();
	dirlight = get_node("DirectionalLight");
	
	env.set_enable_fx(Environment.FX_FXAA, globals.cfg_fxaa);
	env.set_enable_fx(Environment.FX_GLOW, globals.cfg_glow);
	env.set_enable_fx(Environment.FX_FOG, globals.cfg_fog);
